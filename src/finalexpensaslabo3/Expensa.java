/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalexpensaslabo3;

import java.util.ArrayList;

/**
 *
 * @author Maka
 */
public class Expensa {
    private static ArrayList<Gasto> gastos; 
    
    public Expensa() {
        gastos = new ArrayList();
    }
    
    public static ArrayList<Gasto> getGastos() {
        return gastos;
    }
    
    public void agregarGasto(Gasto g){
        gastos.add(g); 
    }
    
    public static double importeAPagarDeptos(){
        int depas = 15;
        double acumulador = 0;
            for (Gasto g : gastos) {
                if(g.toStringTipo().equals("Departamento")) { //dpto
                    acumulador += g.getImporte();
                }
            }
        System.out.println(acumulador/depas+" depas");
        return acumulador/depas;
    }
    
    public static double importeAPagarLocales(){
        int locales = 4;
        double acumulador = 0;
            for (Gasto g : gastos) {
                if(g.toStringTipo().equals("Local")) { //local
                    acumulador += g.getImporte();
                }
            }
        System.out.println(acumulador/locales+" locales");
        return acumulador/locales;
    }
    
    public static ArrayList<Gasto> getGastoMayorA500(){
        ArrayList<Gasto> lista = new ArrayList<>();
        
        for (Gasto g : gastos) {
            if(g.getImporte()>5000){
                lista.add(g);
            }
        }
        return lista;
    }
}
