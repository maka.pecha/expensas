/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalexpensaslabo3;

/**
 *
 * @author Maka
 */
public class Gasto {
    private String concepto;
    private float importe;
    private boolean tipo; //0-dpto 1-local

    public Gasto() {
    }

    public Gasto(String concepto, float importe, boolean tipo) {
        this.concepto = concepto;
        this.importe = importe;
        this.tipo = tipo;
    }

    public String getConcepto() {
        return concepto;
    }

    public float getImporte() {
        return importe;
    }

    public boolean isTipo() {
        return tipo;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }
    
    public String toStringTipo() {
        String tipo= "Departamento";
        if (isTipo() == true) {
            tipo = "Local";
        }
        return tipo;
    }

    @Override
    public String toString() {
        return "expensa{" + "concepto=" + concepto + ", importe=" + importe + ", tipo=" + toStringTipo() + '}';
    }
    
}
